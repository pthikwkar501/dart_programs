abstract class demo{
  demo(){
    print("in constructor demo");

  }

  void fun1(){
    print("in fun 1");
  }

  void fun2();
}

class demochild extends demo{

  demochild(){

    print("in demo child");
  }
  void fun2(){
    print("in fun 2");
  }
}

void main(){

  demochild obj = new demochild();
  obj.fun1();
  obj.fun2();
}