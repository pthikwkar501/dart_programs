class demo{
  int? _x;
  String str;
  double? _sal;

  demo(this._x,this.str,this._sal);
//1st way
 /* int? getx(){
    return _x;
  }

  double? getsal(){
    return _sal;
  }
  
  //2 way

 get getx{
  return _x;
}

 get getsal{
  return _sal;
 }
 */
int? get getx  => _x;
double? get getsal  => _sal;
get getstr => str;



}
