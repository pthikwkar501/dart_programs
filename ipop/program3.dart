import 'dart:core';
import 'dart:io';

void main(){
  int? empId;
  String? name;
  double? salary;

  print("enter employee id:");
  empId = int.parse(stdin.readLineSync()!);

  print("enter employee name:");
  name = stdin.readLineSync();

  print("enter salary:");
  salary = double.parse(stdin.readLineSync()!);

  stdout.writeln("Id:$empId,name:$name,salary:$salary");
}