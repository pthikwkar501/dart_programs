class c2w{
  String? owner;
  int? no_of_student;

  c2w(this.owner,this.no_of_student);

  void courses(){
    print("python");
  }

  void course_price(){
    print("price upto 10 k");
  }
}

class Incubator extends c2w{
  String? owner;
  int? no_of_student;
  int? no_of_courses;

  Incubator(this.owner,this.no_of_student,this.no_of_courses):super(owner,no_of_student);


  void courses(){
    print("Flutter");

  }

  void course_price(){
    print("price upto 20 k");
  }
}

void main(){
  c2w obj1 = new Incubator("prathamesh",200,8);
  obj1.courses();
  obj1.course_price();
}


